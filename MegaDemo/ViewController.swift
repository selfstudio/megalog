//
//  ViewController.swift
//  MegaDemo
//
//  Created by Lincoln Law on 2017/6/5.
//  Copyright © 2017年 Lincoln Law. All rights reserved.
//

import UIKit
import MegaLog
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        MegaLog.xcodeConsoleLogEnable = true
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        print(MegaLogType.none.rawValue)
        MegaLog.verbose(message: "verbose", identifier: "ViewController")
        MegaLog.debug(message: "debug")
        MegaLog.info(message: "info")
        MegaLog.warning(message: "warningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarningwarning")
        MegaLog.error(message: "error")
        MegaLog.showConsole()
    }
}

