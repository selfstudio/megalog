//
//  CAShapeLayer+Pie.swift
//  MegaLog
//
//  Created by Lincoln Law on 2017/6/12.
//  Copyright © 2017年 Lincoln Law. All rights reserved.
//

import Foundation
import QuartzCore
final class PieLayer: CALayer {
    private lazy var pielayers: [MegaLogType: SubPie] = { [unowned self] in
        var map: [MegaLogType: SubPie] = [:]
        let total: [MegaLogType] = [.verbose, .debug, .info, .warning, .error]
        for item in total {
            let shape = SubPie()
            shape.frame = self.bounds
            shape.strokeColor = UIColor.clear
            shape.fillColor = item.color
            map[item] = shape
        }
        return map
    }()

    override init() {
        super.init()
        initialize()
    }

    override init(layer: Any) {
        super.init(layer: layer)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func initialize() {
        frame.size = CGSize(width: 30, height: 30)
        cornerRadius = 15
        borderColor = UIColor.darkGray.cgColor
        borderWidth = 0.3
        for (_, pie) in pielayers {
            addSublayer(pie)
        }
    }

    func update(infos: [MegaLogType: Int]) {
        let total = infos.reduce(Int(0)) { (sum, info) -> Int in return sum + info.1 }

        var currnet: Int = 0
        let pi = Float(Double.pi)
        for (key, value) in pielayers {
            let val = infos[key] ?? 0
            var percentage = Float(currnet) / Float(total)
            var start = CGFloat(percentage * 2 * pi - 0.5 * pi)
            if start.isNaN { start = 0 }
            currnet += val
            percentage = Float(currnet) / Float(total)
            var end = CGFloat(percentage * 2 * pi - 0.5 * pi)
            if end.isNaN { end = 0 }
            value.startAngle = start
            value.endAngle = end
            value.setNeedsDisplay()
        }
    }
}

private final class SubPie: CALayer {
    @NSManaged var startAngle: CGFloat
    @NSManaged var endAngle: CGFloat
    var fillColor: UIColor = .gray
    var strokeWidth: CGFloat = 1
    var strokeColor: UIColor = .black

    override init() {
        super.init()
        contentsScale = UIScreen.main.scale
        allowsEdgeAntialiasing = true
    }

    override init(layer: Any) {
        super.init(layer: layer)
        if let other = layer as? SubPie {
            startAngle = other.startAngle
            endAngle = other.endAngle
            fillColor = other.fillColor
            strokeColor = other.strokeColor
            strokeWidth = other.strokeWidth
        }
    }

    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }

    override func action(forKey event: String) -> CAAction? {
        if event == "startAngle" || event == "endAngle" { return makeAnimation(for: event) }
        return super.action(forKey: event)
    }

    override class func needsDisplay(forKey key: String) -> Bool {
        if key == "startAngle" || key == "endAngle" { return true }
        return super.needsDisplay(forKey: key)
    }

    override func draw(in ctx: CGContext) {
        // Create the path
        ctx.clear(bounds)
        let center = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
        let radius = min(center.x, center.y)

        ctx.beginPath()
        ctx.move(to: center)

        let p1 = CGPoint(x: center.x + radius * CGFloat(cosf(Float(startAngle))), y: center.y + radius * CGFloat(sinf(Float(startAngle))))
        ctx.addLine(to: p1)

        let clockwise = startAngle > endAngle
        ctx.addArc(center: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: clockwise)
        ctx.closePath()

        // Color it
        ctx.setFillColor(fillColor.cgColor)
        ctx.setStrokeColor(strokeColor.cgColor)
        ctx.setLineWidth(strokeWidth)
        ctx.drawPath(using: CGPathDrawingMode.fillStroke)
    }

    private func makeAnimation(for key: String) -> CABasicAnimation {
        let anim = CABasicAnimation(keyPath: key)
        anim.fromValue = presentation()?.value(forKey: key)
        anim.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        anim.duration = 0.3
        return anim
    }
}
