//
//  MegaConsole.swift
//  MegaLog
//
//  Created by Lincoln Law on 2017/6/5.
//  Copyright © 2017年 Lincoln Law. All rights reserved.
//
import Foundation
import PinLayout

final class MegaConsole: UINavigationController {
    deinit {
        print("MegaConsole deinit")
    }
    override init(nibName _: String?, bundle _: Bundle?) {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
    }
    
    convenience init() {
        let vc = MegaConsoleManager()
        self.init(rootViewController: vc)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        style()
    }
    
    private func style() {
        view.backgroundColor = 0x062333.mega_color
        navigationBar.tintColor = 0xFBFCFD.mega_color
        let barBackgroundView = navigationBar.subviews[0]
        let valueForKey = barBackgroundView.value(forKey:)
        let bgg = UIView()
        bgg.backgroundColor = 0x0F2E41.mega_color
        bgg.translatesAutoresizingMaskIntoConstraints = false
        var parent = barBackgroundView
        if navigationBar.isTranslucent {
            if #available(iOS 10.0, *) {
                if let backgroundEffectView = valueForKey("_backgroundEffectView") as? UIVisualEffectView, navigationBar.backgroundImage(for: .default) == nil {
                    parent = backgroundEffectView.contentView
                }
                
            } else {
                if let adaptiveBackdrop = valueForKey("_adaptiveBackdrop") as? UIView, let backdropEffectView = adaptiveBackdrop.value(forKey: "_backdropEffectView") as? UIView {
                    parent = backdropEffectView
                }
            }
        }
        parent.addSubview(bgg)
        parent.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[bgg]-0-|", options: .directionLeftToRight, metrics: nil, views: ["bgg" : bgg]))
        parent.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[bgg]-0-|", options: .directionLeftToRight, metrics: nil, views: ["bgg" : bgg]))
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}

final class MegaConsoleManager: UIViewController {
    
    private var _loading = false {
        didSet {
            DispatchQueue.main.async {
                self._footerView.text = "· \(self._loading ? "loading" : "end") ·"
            }
        }
    }
    private var _lastRead = Int64()
    private var _totalBytes = Int64()
    
    private var _filters: [MegaLogType] = []
    private var _keyword: String = ""
    private var _totalLine: [LogLine] = []
    private var _filterList: [LogCell.Framer] = []
    
    private var _dataParserQueue = DispatchQueue(label: "MegaLog.dataParser", target: DispatchQueue.global())
    private var _fileDscriptor: UnsafeMutablePointer<FILE>?
    private var _bufferLength = 8192
    private var _lastLeftContent = ""
    
    private var _obToken: NSKeyValueObservation?
    private var _ob: NSObjectProtocol?
    private var _lastFilePath: String = ""
    
    private lazy var _tools: UtilsView = { [unowned self] in
        let tools = UtilsView()
        tools.layer.transform = self._closingTransfrom
        tools.frame.size.height = 200
        tools.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
        tools.frame.origin.y = 64
        tools.filterChangedHandler = { [unowned self] filters in
            self.filterDidChanged(to: filters)
        }
        tools.fileChangedHandler = {[unowned self] path in
            guard self._lastFilePath != path else { return }
            self._lastFilePath = path
            self.fileDidChanged(to: path)
        }
        return tools
    }()
    
    private lazy var _fileHint: FileHint = {[unowned self] in
        let label = FileHint()
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12, weight: .light)
        label.textColor = UIColor.lightGray
        let tap = UITapGestureRecognizer(target: self, action: #selector(MegaConsoleManager.togglePie))
        label.addGestureRecognizer(tap)
        return label
    }()
    
    private lazy var _searchBar: SearchBar = { [unowned self] in
        let search = SearchBar(frame: CGRect(x: 10, y: 20, width: 250, height: 28))
        search.translatesAutoresizingMaskIntoConstraints = false
        search.delegate = self
        return search
        }()
    
    private var _footerView: UILabel = {
        let label = UILabel()
        label.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12, weight: .light)
        label.textColor = UIColor.lightGray
        label.text = "· loading ·"
        return label
    }()
    
    private lazy var _tableView: UITableView = {[unowned self] in
        let cv = UITableView(frame: .zero, style: .plain)
        cv.delegate = self
        cv.dataSource = self
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.backgroundColor = UIColor.clear
        cv.estimatedRowHeight = 100
        cv.contentInset.bottom = 20
        cv.separatorStyle = .none
        cv.register(LogCell.self, forCellReuseIdentifier: LogCell.identifier)
        cv.tableFooterView = self._footerView
        return cv
    }()
    
    private lazy var _pielayer: PieLayer = {[unowned self] in
        let layer = PieLayer()
        return layer
        }()
    
    private lazy var _pieButton: UIView = {[unowned self] in
        let view = UIView(frame: CGRect.zero)
        view.layer.addSublayer(self._pielayer)
        view.frame.size = self._pielayer.frame.size
        let tap = UITapGestureRecognizer(target: self, action: #selector(MegaConsoleManager.togglePie))
        view.addGestureRecognizer(tap)
        return view
        }()
    
    private lazy var _closingTransfrom: CATransform3D = {
        var closingT = CATransform3DMakeRotation(CGFloat(Double.pi/2), 1, 0, 0)
        closingT.m34 = -1 / 500
        return closingT
    }()
    
    private var _currentLogText: NSAttributedString?
    
    deinit {
        _obToken?.invalidate()
        NotificationCenter.default.removeObserver(self)
        print(#file,#function)
    }
    override init(nibName _: String?, bundle _: Bundle?) { super.init(nibName: nil, bundle: nil) }
    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    public convenience init() { self.init(nibName: nil, bundle: nil) }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(_tableView)
        view.addSubview(_tools)
        view.addSubview(_fileHint)
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[_tableView]-0-|", options: .directionLeftToRight, metrics: nil, views: ["_tableView" : _tableView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[_tableView]-0-|", options: .directionLeftToRight, metrics: nil, views: ["_tableView" : _tableView]))
        
        view.backgroundColor = 0x062131.mega_color
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(MegaConsoleManager.close))
        navigationItem.titleView = _searchBar
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: _pieButton)
        _lastFilePath = "\(MegaLog.folder)/\(MegaLog.currnetLog)"
        initFD(with: _lastFilePath)
        _tableView.ss_leadingScreensForBatching = 1.5
        _tableView.setBatchDelegate(self)
        _obToken = _tableView.observe(\UITableView.contentOffset) {(table, _) in
            table.offsetChange()
        }
        _ob = NotificationCenter.default.addObserver(forName: .tips, object: nil, queue: nil) {[unowned self] (note) in
            guard let message = note.object as? String else { return }
            DispatchQueue.main.async {
                self._fileHint.text  = message
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    self._fileHint.attributedText = self._currentLogText
                })
            }
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let isLanscape = view.bounds.width > view.bounds.height
        _tools.frame.origin.y = isLanscape ? 44 : 64
        _tools.frame.size = CGSize(width: view.bounds.width, height: 200)
        _fileHint.pin.left().right().bottom().height(20)
    }
    
    private func utilsOpening() -> Bool {
        let t = _tools.layer.transform
        let isOpening = t.m11 == t.m22 && t.m22 == t.m33 && t.m33 == t.m44 && t.m11 == 1
        return isOpening
    }
    @objc private func togglePie() {
        let isOpening = utilsOpening()
        isOpening ? hidePie() : showPie()
    }
    
    private func showPie() {
        guard utilsOpening() == false else { return }
        UIView.animate(withDuration: 0.3) {
            self._tools.layer.transform = CATransform3DIdentity
        }
    }
    
    private func hidePie() {
        guard utilsOpening() else { return }
        UIView.animate(withDuration: 0.3) {
            self._tools.layer.transform = self._closingTransfrom
        }
    }
    
    private func initFD(with fileName: String) {
        _currentLogText = UtilsView.file(with: fileName, iconBigSize: false, newLine: false, replacingExtension: false, textColor: UIColor.lightGray)
        _fileHint.attributedText = _currentLogText
        if let fd = _fileDscriptor {
            fclose(fd)
            _fileDscriptor = nil
            _totalBytes = 0
            _lastRead = 0
        }
        var buffer = stat()
        if stat(fileName.withCString({ $0 }), &buffer) == 0 {
            _totalBytes = buffer.st_size
            _lastRead = 0
            let fd = fopen(fileName, "r")
            _fileDscriptor = fd
        }
        _loading = false
        self.read()
    }
    
    private func realRead(done: @escaping () -> ()) {
        _loading = true
        defer { _loading = false }
        guard let file = _fileDscriptor else {
            done()
            return
        }
        let lenght64 = Int64(_bufferLength)
        var length = _bufferLength
        if _totalBytes - lenght64 > 0 {
            _totalBytes -= lenght64
        } else {
            length = Int(_totalBytes)
            _totalBytes = 0
        }
        
        var bytes: [UInt8] = Array(repeating: 0, count: length)
        fseek(file, Int(_totalBytes), SEEK_SET)
        fread(&bytes, 1, length, file)
        if _totalBytes == 0 {
            fclose(file)
            _fileDscriptor = nil
        }
        guard var content = String(bytes: bytes, encoding: .utf8) else {
            done()
            return
        }
        if _lastLeftContent.isEmpty == false {
            content = "\(content)\(_lastLeftContent)"
            _lastLeftContent = ""
        }
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        
        let lines = content.components(separatedBy: MegaLog.seperatorLine).flatMap({ (line) -> LogLine? in
            let raw = line.trimmingCharacters(in: .whitespacesAndNewlines)
            guard raw.isEmpty == false, let data = raw.data(using: .utf8) else { return nil }
            do {
                if raw.hasPrefix("{") == false {
                    _lastLeftContent = raw
                    return nil
                }
                let result = try decoder.decode(LogLine.self, from: data)
                return result
            } catch {
                print(content)
                print(raw)
                print(error)
                return nil
            }
        })
        let newlines = Array(lines.reversed())
        _totalLine += newlines
        let cals = newlines.flatMap({ (line) -> LogCell.Framer? in
            var hasType = true
            var hasWord = true
            if _filters.isEmpty == false {
                hasType = self._filters.contains(line.type)
            }
            if _keyword != "" {
                hasWord = line.searchContent.lowercased().contains(_keyword.lowercased())
            }
            if hasType, hasWord {
                return LogCell.Framer.from(line: line)
            }
            return nil
        })
        
        var rows: [IndexPath] = []
        let old = self._filterList.count
        let final = self._filterList + cals
        
        var frequencies: [MegaLogType: Int] = [:]
        for line in final {
            frequencies[line.line.type, default: 0] += 1
        }
        
        if old == 0 {
            DispatchQueue.main.async {
                self._pielayer.update(infos: frequencies)
                self._filterList = final
                self._tableView.reloadData()
                done()
            }
            return
        }
        let to = old + cals.count
        for i in old..<to {
            let path = IndexPath(row: i, section: 0)
            rows.append(path)
        }
        
        DispatchQueue.main.async {
            self._pielayer.update(infos: frequencies)
            self._filterList = final
            done()
            self._tableView.beginUpdates()
            self._tableView.insertRows(at: rows, with: .automatic)
            self._tableView.endUpdates()
        }
        
    }
    private func read(done: @escaping () -> () = { }) {
        DispatchQueue.global(qos: .userInitiated).async {
            self._dataParserQueue.sync {
                self.realRead(done: done)
            }
        }
    }
    
    @objc private func close() {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    private func filterDidChanged(to filters: [MegaLogType]) {
        _filters = filters
        dataChanged()
    }
    
    private func fileDidChanged(to path: String) {
        _loading = true
        _lastLeftContent = ""
        _totalLine = []
        _filterList = []
        _tableView.reloadData()
        _tableView.setContentOffset(CGPoint(x:0, y: -64), animated: false)
        initFD(with: path)
    }
    
    private func dataChanged() {
        DispatchQueue.global(qos: .userInitiated).async {
            var final: [LogLine] = []
            if self._filters.isEmpty {
                final = self._totalLine
            } else {
                final = self._totalLine.filter({ (line) -> Bool in
                    self._filters.contains(line.type)
                })
            }
            let word = self._keyword
            if word.count > 0 {
                final = final.filter({ (line) -> Bool in
                    line.searchContent.lowercased().contains(word.lowercased())
                })
            }
            let cells = final.flatMap({ (line) -> LogCell.Framer? in
                return LogCell.Framer.from(line: line)
            })
            
            var frequencies: [MegaLogType: Int] = [:]
            for line in final {
                frequencies[line.type, default: 0] += 1
            }
            DispatchQueue.main.async {
                self._pielayer.update(infos: frequencies)
                self._filterList = cells
                self._tableView.reloadData()
                self._tableView.setContentOffset(CGPoint(x:0, y: -64), animated: false)
            }
        }
    }
}
extension MegaConsoleManager: ScrollviewBatchFetchingable {
    func scrollView(_ scrollView: UIScrollView, willBeginBatchFetchWithContext context: SSBatchContext) {
        guard _loading == false else {
            context.completeBatchFetching(true)
            return
        }
        read { context.completeBatchFetching(true) }
    }
}
extension MegaConsoleManager: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in _: UITableView) -> Int { return 1 }
    
    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return _filterList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: LogCell.identifier) as! LogCell
        if let data = _filterList[safe: indexPath.row] {
            cell.update(data: data, keyword: _keyword)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = _filterList[safe: indexPath.row]?.total {
            return height
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hidePie()
        _searchBar.resignFirstResponder()
    }
}
// MARK: UITextFieldDelegate
extension MegaConsoleManager: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let text = textField.text {
            _keyword = text
            dataChanged()
        }
        hidePie()
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        DispatchQueue.main.async { self.showPie() }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        _keyword = ""
        dataChanged()
        return true
    }
}

private final class LogCell: UITableViewCell {
    
    private struct MagicNumber {
        static let margin: CGFloat = 2
        static let nameHeight: CGFloat = 16
        static let indicatorWidth: CGFloat = 4
        static let indicatorNameGap: CGFloat = 4
    }
    
    struct Framer {
        
        let contentHeight: CGFloat
        let moreHeight: CGFloat
        let content: NSAttributedString
        let more: NSAttributedString
        let name: NSAttributedString
        let line: LogLine
        let total: CGFloat
        init(contentHeight: CGFloat, moreHeight: CGFloat, content: NSAttributedString, more: NSAttributedString, name: NSAttributedString, line: LogLine) {
            self.contentHeight = contentHeight
            self.moreHeight = moreHeight
            self.content = content
            self.more = more
            self.name = name
            self.line = line
            total = Framer.fixedHeight + contentHeight + moreHeight
        }
        static let fixedHeight = CGFloat(MagicNumber.margin + MagicNumber.nameHeight + MagicNumber.margin)
        static let fixedOffsetX = CGFloat(MagicNumber.margin + MagicNumber.indicatorWidth + MagicNumber.indicatorNameGap + MagicNumber.margin)
        enum Style {
            case content, more, name
            var info: (CGFloat, Int) {
                switch self {
                case .content: return (14, 0x9EADB3)
                case .more: return (12, 0x654273)
                case .name: return (12, 0x456273)
                }
            }
        }
        
        static func attributedString(with txt: String, for style: Style) -> NSAttributedString {
            let (fontSize, colorRaw) = style.info
            let color = colorRaw.mega_color
            var font = UIFont.systemFont(ofSize: fontSize, weight: .light)
            var att: [NSAttributedStringKey : Any] = [:]
            if #available(iOS 11, *) {
                font = UIFontMetrics(forTextStyle: UIFontTextStyle.body).scaledFont(for: font)
            }
            att[NSAttributedStringKey.font] = font
            att[NSAttributedStringKey.foregroundColor] = color
            return NSAttributedString(string: txt, attributes: att)
        }
        
        
        static func from(line: LogLine) -> Framer {
            let width = UIScreen.main.bounds.width - fixedOffsetX
            let content = attributedString(with: line.message, for: .content)
            let size = CGSize(width: width, height: CGFloat.infinity)
            let opts: NSStringDrawingOptions = [NSStringDrawingOptions.usesFontLeading, NSStringDrawingOptions.usesLineFragmentOrigin]
            let rect = content.boundingRect(with: size, options: opts, context: nil)
            let contentHeight = ceil(rect.height)
            let moreRaw = """
            \(line.method) \((line.file as NSString).lastPathComponent) at \(line.line)
            \(line.thread)
            """
            let more = attributedString(with: moreRaw, for: .more)
            let rectmore = more.boundingRect(with: size, options: opts, context: nil)
            let moreHeight = ceil(rectmore.height)
            let nameRaw = line.identifier.count > 0 ? line.identifier : line.type.rawValue
            let name = attributedString(with: nameRaw, for: .name)
            return Framer(contentHeight: contentHeight, moreHeight: moreHeight, content: content, more: more, name: name, line: line)
        }
    }
    
    private lazy var _indicator: UIView = {
        let layer = UIView()
        layer.layer.cornerRadius = 2
        layer.clipsToBounds = true
        layer.translatesAutoresizingMaskIntoConstraints = false
        return layer
    }()
    
    private lazy var _name: UILabel = {
        let label = UILabel()
        label.backgroundColor = 0x062333.mega_color
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var _time: UILabel = {
        let label = UILabel()
        label.textColor = 0x456273.mega_color
        var font = UIFont.systemFont(ofSize: 12, weight: .light)
        var att: [NSAttributedStringKey : Any] = [:]
        if #available(iOS 11, *) {
            font = UIFontMetrics(forTextStyle: UIFontTextStyle.body).scaledFont(for: font)
        }
        label.font = font
        label.textAlignment = .right
        label.backgroundColor = 0x062333.mega_color
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var _content: UILabel = {
        let label = UILabel()
        label.textColor = 0x9EADB3.mega_color
        label.backgroundColor = 0x062333.mega_color
        label.numberOfLines = 0
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var _more: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.backgroundColor = 0x062333.mega_color
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var _line: LogLine?
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        backgroundColor = 0x062333.mega_color
        contentView.removeFromSuperview()
        selectionStyle = .none
        addSubview(_indicator)
        addSubview(_name)
        addSubview(_time)
        addSubview(_content)
        addSubview(_more)
        let color = 0x062333.mega_color
        backgroundColor = color
        _name.backgroundColor = color
        _time.backgroundColor = color
        _content.backgroundColor = color
        _more.backgroundColor = color
        let press = UILongPressGestureRecognizer(target: self, action: #selector(LogCell.longPress(p:)))
        addGestureRecognizer(press)
    }
    
    @objc private func longPress(p: UILongPressGestureRecognizer) {
        guard p.state == .ended else { return }
        becomeFirstResponder()
        let copyItem = UIMenuItem(title: "Copy Log", action: #selector(LogCell.copyInfo))
        let menuController = UIMenuController.shared
        if menuController.isMenuVisible {
            menuController.setMenuVisible(false, animated: false)
        }
        menuController.menuItems = [copyItem]
        menuController.setTargetRect(bounds.insetBy(dx: 0, dy: bounds.height/2), in: self)
        menuController.setMenuVisible(true, animated: true)
    }
    @objc private func copyInfo() {
        guard let line = _line?.description else { return }
        resignFirstResponder()
        UIPasteboard.general.string = line
        NotificationCenter.default.post(name: .tips, object: "Copy Success")
    }
    override var canBecomeFirstResponder: Bool { return true }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return action == #selector(LogCell.copyInfo)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let margin = MagicNumber.margin
        let indicatorWidth = MagicNumber.indicatorWidth
        let gap = MagicNumber.indicatorNameGap
        let nameHeight = MagicNumber.nameHeight
        _indicator.pin.top(margin).left(margin).width(indicatorWidth).bottom(margin)
        _name.pin.right(of: _indicator, aligned: .top).marginHorizontal(gap).width(70%).height(nameHeight)
        _time.pin.right(of: _name, aligned: .top).right(margin).height(nameHeight)
        _content.pin.below(of: _name, aligned: .left).right(margin).height(_content.frame.height)
        _more.pin.below(of: _content, aligned: .left).right(margin).height(_more.frame.height)
    }
    
    func update(data: Framer, keyword: String) {
        _line = data.line
        let type = data.line.type
        _content.pin.below(of: _name, aligned: .left).right(MagicNumber.margin).height(data.contentHeight)
        _more.pin.below(of: _content, aligned: .left).right(MagicNumber.margin).height(data.moreHeight)
        _indicator.backgroundColor = type.color
        _name.attributedText = data.name
        _time.text = data.line.ago
        _content.attributedText = data.content
        _more.attributedText = data.more
        let total = [_content, _more, _name]
        for item in total { item.highlight(word: keyword) }
    }
    
    
}

struct LogLine: Codable, CustomStringConvertible {
    let type: MegaLogType
    let date: Date
    let identifier: String
    let message: String
    let file: String
    let method: String
    let line: Int
    let thread: String
    
    var description: String {
        return "\(searchContent)\n\(date)"
    }
    
    var searchContent: String {
        return """
        \(type.rawValue)
        \(identifier)
        \(message)
        \(file)
        \(method)
        \(line)
        \(thread)
        """
    }
    
    var ago: String { return date.timeAgoSinceNow() }
}

private final class UtilsView: UIView {
    
    static let height: CGFloat = 200
    private lazy var _filterButtons: [MegaLogType : FilterButton] = {
        var buttons: [MegaLogType : FilterButton] = [:]
        let all: [MegaLogType] = [.debug, .info, .verbose, .warning, .error]
        for item in all {
            let b = FilterButton(type: item)
            b._selected = true
            buttons[item] = b
        }
        return buttons
    }()
    
    var filterChangedHandler: ([MegaLogType]) -> Void = { _ in }
    var fileChangedHandler: (String) -> Void = { _ in }
    
    private lazy var _currentFileLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.backgroundColor = 0x062333.mega_color
        label.layer.cornerRadius = 4
        label.clipsToBounds = true
        return label
    }()
    
    private lazy var _currentFileLabelTip: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textAlignment = .center
        label.backgroundColor = 0x062333.mega_color
        label.layer.cornerRadius = 4
        label.clipsToBounds = true
        label.text = "current"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 11, weight: .light)
        return label
    }()
    
    private lazy var _collectionView: UICollectionView = {
        let cv = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        cv.backgroundColor = 0x0F2E41.mega_color
        cv.register(LogFileCell.self, forCellWithReuseIdentifier: LogFileCell.identifier)
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    private var _fileList: [String] = []
    private var _itemSize: CGSize = .zero
    private var _token: NSObjectProtocol?
    override init(frame: CGRect) { super.init(frame: frame) }
    
    required init?(coder aDecoder: NSCoder) { super.init(coder: aDecoder) }
    
    deinit { NotificationCenter.default.removeObserver(self) }
    
    convenience init() {
        self.init(frame: .zero)
        frame.size.height = 200
        initialize()
    }
    
    private func initialize() {
        backgroundColor = 0x0F2E41.mega_color
        
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 4
        layer.shadowOpacity = 0.8
        
        for (_, button) in _filterButtons { addSubview(button) }
        addSubview(_currentFileLabel)
        addSubview(_currentFileLabelTip)
        addSubview(_collectionView)
        
        _currentFileLabel.attributedText = UtilsView.file(with: MegaLog.currnetLog)
        
        let fm = FileManager.default
        do {
            let contents = try fm.contentsOfDirectory(atPath: MegaLog.folder)
            _fileList = contents.filter({ (folder) -> Bool in
                return folder != ".DS_Store"
            })
            _collectionView.reloadData()
        } catch {
            print("get folder contents error:\(error)")
        }
        
        _token = NotificationCenter.default.addObserver(forName: .filterDidChanged, object: nil, queue: nil) {[unowned self] (_) in
            var types = self._filterButtons.flatMap({ (info) -> MegaLogType? in
                return  info.value._selected ? info.key : nil
            })
            if types.count == 0 { types = MegaLogType.all }
            self.filterChangedHandler(types)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        guard
            let info = _filterButtons[.info],
            let verbose = _filterButtons[.verbose],
            let debug = _filterButtons[.debug],
            let warning = _filterButtons[.warning],
            let error = _filterButtons[.error] else { return }
        let count = CGFloat(_filterButtons.count)
        let margin: CGFloat = 2
        let topMargin: CGFloat = 6
        let width = floor((bounds.width - (count + 1) * margin) / count)
        let hMargin: CGFloat = (bounds.width - (width * count + margin * (count - 1))) / 2
        let height = CGFloat(44)
        info.pin.left(hMargin).top(topMargin).width(width).height(height)
        verbose.pin.right(of: info, aligned: .top).marginHorizontal(2).width(width).height(height)
        debug.pin.right(of: verbose, aligned: .top).marginHorizontal(2).width(width).height(height)
        warning.pin.right(of: debug, aligned: .top).marginHorizontal(2).width(width).height(height)
        error.pin.right(of: warning, aligned: .top).marginHorizontal(2).width(width).height(height)
        _currentFileLabelTip.pin.left(hMargin).width(100).height(20).bottom(topMargin)
        _currentFileLabelTip.frame.origin.y = UtilsView.height - 20 - topMargin
        _currentFileLabel.pin.below(of: info, aligned: .left).width(100).bottomRight(to: _currentFileLabelTip.anchor.topRight).marginTop(topMargin).marginBottom(4)
        let h = _currentFileLabelTip.frame.maxY - _currentFileLabel.frame.minY
        _collectionView.pin.right(of: _currentFileLabel, aligned: .top).marginLeft(4).topRight(to: error.anchor.bottomRight).height(h)
        _itemSize = CGSize(width: (_collectionView.frame.width - 12) / 2, height: 30)
        _collectionView.collectionViewLayout.invalidateLayout()
    }
    
    
    fileprivate static func file(with name: String, iconBigSize: Bool = true, newLine: Bool = true, replacingExtension: Bool = true, textColor: UIColor = .white) -> NSAttributedString {
        let size = iconBigSize ? 60 : 11
        let fileAtt = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: CGFloat(size))]
        let fileTxt = NSAttributedString(string: "📝", attributes: fileAtt)
        let att = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 11, weight: .light), NSAttributedStringKey.foregroundColor : textColor]
        var raw = name
        if replacingExtension { raw = name.replacingOccurrences(of: ".log", with: "") }
        let txt = NSMutableAttributedString(string: "\(newLine ? "\n" : "")\(raw)", attributes: att)
        txt.insert(fileTxt, at: 0)
        return txt
    }
}

extension UtilsView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int { return 1 }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return _fileList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LogFileCell.identifier, for: indexPath) as! LogFileCell
        if let name = _fileList[safe: indexPath.item] {
            cell.update(with: name)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return _itemSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let name = _fileList[safe: indexPath.item] else { return }
        fileChangedHandler((MegaLog.folder as NSString).appendingPathComponent(name))
        _currentFileLabel.attributedText = UtilsView.file(with: name)
    }
}
private final class LogFileCell: UICollectionViewCell {
    
    private lazy var _label: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        contentView.removeFromSuperview()
        addSubview(_label)
        backgroundColor = 0x062333.mega_color
        layer.cornerRadius = 4
        layer.masksToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        _label.pin.topLeft().bottomRight().margin(4)
    }
    
    func update(with item: String) {
        _label.attributedText = UtilsView.file(with: item, iconBigSize: false, newLine: false)
    }
    
}
extension NSNotification.Name {
    static let filterDidChanged = Notification.Name("FilterCell.FilterDidChanged")
    static let tips = Notification.Name("MegaLog.Tips")
}
private final class FilterButton: UIView {
    
    var type: MegaLogType = .none {
        didSet {
            _indicator.backgroundColor = type.color
            _label.text = type.text
        }
    }
    
    fileprivate var _selected = false {
        didSet {
            let width = _selected ? bounds.width : 6
            UIView.animate(withDuration: 0.15) {
                self._indicator.frame.size.width = width
            }
            _label.textColor = _selected ? 0x0F2E41.mega_color : type.color
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    convenience init(type: MegaLogType) {
        self.init(frame: .zero)
        ({
            self.type = type
            initialize()
        })()
    }
    
    private var _indicator: UIView = {
        let indicator = UIView()
        return indicator
    }()
    
    private var _label: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.light)
        label.textAlignment = .center
        return label
    }()
    
    private func initialize() {
        addSubview(_indicator)
        addSubview(_label)
        backgroundColor = 0x062333.mega_color
        layer.cornerRadius = 4
        layer.borderColor = type.color.cgColor
        layer.borderWidth = 0.5
        layer.masksToBounds = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(FilterButton.tap))
        addGestureRecognizer(tap)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let width = _selected ? bounds.width : 6
        _indicator.pin.top().left().bottom().width(width)
        _label.pin.top().left(6).bottom().right()
    }
    @objc private func tap() {
        _selected = !_selected
        NotificationCenter.default.post(name: .filterDidChanged, object: nil)
    }
}
// MARK: - FiltHint
private final class FileHint: UILabel {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    convenience init() {
        self.init(frame: .zero)
        initialize()
    }
    
    private let line = CALayer()
    
    private func initialize() {
        backgroundColor = 0x062131.mega_color
        lineBreakMode = .byTruncatingMiddle
        clipsToBounds = true
        isUserInteractionEnabled = true
        layer.addSublayer(line)
        line.backgroundColor = UIColor.darkGray.cgColor
        line.frame.size.height = 0.3
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        line.frame.origin.y = 0
        line.frame.origin.x = 0
        line.frame.size.width = bounds.width
    }
}
// MARK: - SearchBar
private final class SearchBar: UITextField {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    private func initialize() {
        let afont = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.light)
        tintColor = .white
        textAlignment = .center
        typingAttributes = [:]
        attributedPlaceholder = NSAttributedString(string: "Search Or Filter", attributes: [NSAttributedStringKey.font: afont, NSAttributedStringKey.foregroundColor: 0x959595.mega_color])
        clearButtonMode = .whileEditing
        backgroundColor = 0xDDDDDD.mega_color.withAlphaComponent(0.1)
        textColor = .lightGray
        font = afont
        layer.cornerRadius = 6
        layer.masksToBounds = true
        returnKeyType = .search
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 5)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 5)
    }
}

// MARK: - extension
extension UITableViewCell { public class var identifier: String { return "\(self)" } }
extension UICollectionViewCell { public class var identifier: String { return "\(self)" } }
extension Int {
    private func color(red: Int, green: Int, blue: Int) -> UIColor {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        return UIColor.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    var mega_color: UIColor {
        return color(red: (self >> 16) & 0xFF, green: (self >> 8) & 0xFF, blue: self & 0xFF)
    }
}
extension Array {
    subscript(safe index: Int) -> Element? {
        return indices ~= index ? self[index] : nil
    }
}
extension UILabel {
    
    func highlight(word: String) {
        guard let att = attributedText, let regex = try? NSRegularExpression(pattern: word, options: NSRegularExpression.Options.caseInsensitive) else { return }
        let raw = att.string
        let results = regex.matches(in: raw, options: [], range: NSMakeRange(0, raw.count))
        let final = NSMutableAttributedString(attributedString: att)
        for result in results {
            let range = result.range
            final.addAttributes([NSAttributedStringKey.backgroundColor : UIColor.yellow], range: range)
        }
        attributedText = final
    }
}

