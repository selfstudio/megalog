//
//  MegaLog.swift
//  MegaLog
//
//  Created by Lincoln Law on 2017/6/5.
//  Copyright © 2017年 Lincoln Law. All rights reserved.
//

import Foundation
public enum MegaLogType: String, Codable {
    case verbose = "💬", debug = "🤖", info = "🚦", warning = "⚠️", error = "⛑", none
    var color: UIColor {
        var c = UIColor.white
        switch self {
        case .none: c = UIColor.white
        case .verbose: c = 0x9FBCCD.mega_color
        case .debug: c = 0x22A37C.mega_color
        case .info: c = 0x3CBBD0.mega_color
        case .warning: c = 0xFBC941.mega_color
        case .error: c = 0xDA2A44.mega_color
        }
        return c
    }
    
    var text: String {
        var value = ""
        switch self {
        case .none: value = "None"
        case .verbose: value = "Verbose"
        case .debug: value = "Debug"
        case .info: value = "Info"
        case .warning: value = "Warning"
        case .error: value = "Error"
        }
        return value
    }
    
    static let all: [MegaLogType] = [.verbose, .debug, .info, .warning, .error]
}

public final class MegaLog {
    static let shared = MegaLog()
    static let seperatorLine = "|rn|"
    
    fileprivate var _writeQueue = DispatchQueue.global(qos: .userInitiated)
    fileprivate var _xcodeConsoleLogEnable = false
    fileprivate var _logQueue = DispatchQueue(label: "com.SelfStudio.Freeplayer.logger")
    fileprivate var _fileHandle: FileHandle?
    fileprivate var _folder = ""
    private var _observer: NSObjectProtocol?
    
    deinit {
        _fileHandle?.closeFile()
        if let ob = _observer {
            NotificationCenter.default.removeObserver(ob)
        }
    }
    
    private init() {
        resetFileHandle()
        DispatchQueue.global(qos: .utility).setTarget(queue: _logQueue)
        _observer = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillTerminate, object: nil, queue: OperationQueue.main) {[unowned self] (_) in
            self._fileHandle?.closeFile()
        }
    }
    
}
// MARK: - FileHandle
extension MegaLog {
    
    private func resetFileHandle() {
        let cache = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first!
        let megaDir = (cache as NSString).appendingPathComponent("MegaLog")
        _folder = megaDir
        let fm = FileManager.default
        if !fm.fileExists(atPath: megaDir) {
            try? fm.createDirectory(atPath: megaDir, withIntermediateDirectories: false, attributes: nil)
        }
        let file = "\(megaDir)/\(currentName)"
        let url = URL(fileURLWithPath: file)
        _fileHandle?.closeFile()
        do {
            if access(file.withCString({$0}), F_OK) == -1 { // file not exists
                FileManager.default.createFile(atPath: file, contents: nil, attributes: nil)
            }
            _fileHandle = try FileHandle(forWritingTo: url)
            _fileHandle?.seekToEndOfFile()
        } catch { print(error) }
    }
    
    public static var currnetLog: String { return MegaLog.shared.currentName }
    
    private var currentName: String {
        let date = Date()
        let fmt = DateFormatter()
        fmt.dateFormat = "yyyy-MM-dd HH:mm:ss"
        fmt.locale = Locale.current
        let total = fmt.string(from: date).components(separatedBy: " ")
        return "\(total.first ?? "").log"
    }
}
extension MegaLog {
    // MARK: - console
    public static func showConsole() {
        let navi = MegaConsole()
        UIApplication.shared.keyWindow?.rootViewController?.showDetailViewController(navi, sender: nil)
    }
    // MARK: - var
    public static var xcodeConsoleLogEnable: Bool {
        get { return MegaLog.shared._xcodeConsoleLogEnable }
        set { MegaLog.shared._xcodeConsoleLogEnable = newValue }
    }
    
    public static var executeQueue: DispatchQueue {
        get { return MegaLog.shared._writeQueue }
        set { MegaLog.shared._writeQueue = newValue }
    }
    
    public static var folder: String { return MegaLog.shared._folder }
    
    // MARK: - log
    private static func log(identifier: String, message: String, type: MegaLogType, file: String, method: String, line: Int) {
        let date = Date()
        let thread = Thread.current
        MegaLog.shared._writeQueue.async {
            MegaLog.shared._logQueue.sync  {
                if MegaLog.shared._xcodeConsoleLogEnable {
                    print("\(type.rawValue):\(message)\n{\n\t\(date) \n\t\(identifier) \n\t\(method) \n\tline \(line) in \(file) \n\t\(thread.description)\n}")
                }
                let total = """
                {
                    "type": "\(type.rawValue)",
                    "date": \(date.timeIntervalSince1970),
                    "identifier": "\(identifier)",
                    "message": "\(message)",
                    "file": "\(file)",
                    "method": "\(method)",
                    "line": \(line),
                    "thread": "\(thread.description)"
                }\(MegaLog.seperatorLine)
                """
                guard let data = total.data(using: .utf8) else { return }
                MegaLog.shared._fileHandle?.write(data)
            }
        }
    }
    
    public static func verbose(message: String, identifier: String = "", file: String = #file, method: String = #function, line: Int = #line) {
        log(identifier: identifier, message: message, type: .verbose, file: file, method: method, line: line)
    }
    
    public static func debug(message: String, identifier: String = "", file: String = #file, method: String = #function, line: Int = #line) {
        log(identifier: identifier, message: message, type: .debug, file: file, method: method, line: line)
    }
    
    public static func info(message: String, identifier: String = "", file: String = #file, method: String = #function, line: Int = #line) {
        log(identifier: identifier, message: message, type: .info, file: file, method: method, line: line)
    }
    
    public static func warning(message: String, identifier: String = "", file: String = #file, method: String = #function, line: Int = #line) {
        log(identifier: identifier, message: message, type: .warning, file: file, method: method, line: line)
    }
    
    public static func error(message: String, identifier: String = "", file: String = #file, method: String = #function, line: Int = #line) {
        log(identifier: identifier, message: message, type: .error, file: file, method: method, line: line)
    }
    
}
